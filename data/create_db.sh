#!/bin/bash

for i in `seq 1 500`;
do
  lname="$(shuf -n1 people.csv|cut -d\, -f2)"
  name="$(shuf -n1 people.csv|cut -d\, -f3) $lname"
  id="$(echo $(date) $name | md5sum|awk '{print $1}')"
  pname="$(shuf -n1 people.csv|cut -d\, -f3) $lname"
  pname2="$(shuf -n1 people.csv|cut -d\, -f3) $lname"
  address="$(shuf -n1 people.csv|cut -d\, -f4,5,6)"
  phone="(555)$((100 + RANDOM % 800))-$((1000 + RANDOM % 8000))"

  echo "$id,$name,$pname,$pname2,$address,$phone,$((RANDOM % 5))"
done > children.csv
